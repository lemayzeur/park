Map<String, dynamic> intros = {
  'ht': {
    'title':'BYENVINI NAN PAK NASYONAL ISTORIK - SITADÈL, SANSOUSI, RAMYE',
    'description':'Ou jwenn Pak Nasyonal Istorik la nan pati Nò peyi Dayiti. Li gen ladan l – Sitadèl la, Sansousi, Ramye. Li chita nan mitan zòn masif Nò a ki rive jouk Ladominikani. (PNH- CSSR) Pak la chita nan lizyè plèn, bò lanmè ak montay sou anndan. Chwa ki te fèt pou bati Sitadèl la sou tèt mòn anlè sa a, se yon rezon estrateji ak lojik pou pwoteje peyi a alenteryè. Sa ki te diferan pou sistèm defans ki te fèt bò lanmè, yo te eritye nan men kolon fransè yo.\n\nSe yon dekrè prezidansyèl nan lane 1978 ki te pran pou yo te ka pwoteje kalite bèl tèt mòn sa a, ki te kouvri ak bèl pye bya byen kanpe. Pak la (PNH-CSSR) sou yon tèren ki mezire 25 km2 (kilomèt kare). Li gen ladan l yon ekip moniman ki nan palè Sansousi ak ozanviwon l, Sitadèl Henry a ak lokal Ramye a, reprezante senbòl inivèsèl libète, paske se te premye moniman esklav nwa yo ki te soti an Afrik te bati lè yo fin konbat pou libète yo. Pou Ayisyen, yo reprezante moniman ki gen plis siyifikasyon pou endepandans yo. \n\nMoniman sa yo an Ayiti, te fèt nan dat soti lane 1800 yo, ki se dat ki make nesans Nasyon Ayisyen an, epitou yo reprezante yon ekip pyès byen monte nan anviwonman natirèl yo. Yo chaje ak senbòl inivèsèl ki ap jistifye chwa yo fè pou mete Pak Nasyonal Istorik la nan lis Eritaj Mondyal UNESCO a.\n\nSe pou lemondantye yo ye.',
    "plan":"25 km 2 (Kilomèt kare)\nApeprè 970 mèt wotè\nTan w’ap pran pou visite li: Pou pi piti katrèdtan w’ap pran pou vizite tout moniman nan Pak la, soti nan Sansousi pou rive Sitadèl la.\nApeprè 45 a 60 minit pou monte rive nan Sitadèl la.\nSoti Sansousi pou rive Choiseul: 4.4 km\nSoti Choiseul pou rive Sitadèl la: 1.6 km\nSoti Sitadèl la pou rive Ramye: 1 km"
  },
  'en': {
    'title':'WELCOME TO THE NATIONAL HISTORIC PARK CITADELLE, SANS-SOUCI, RAMIERS',
    'description':'Located in the northern part of the Republic of Haiti, the National Historic Park – Citadelle, Sans Souci, Ramiers (PNH-CSSR) falls within the central zone of the northern massifs which extend into the Dominican Republic. The PNH-CSSR is located at the hinge of the plain, coastal and mountainous toward the inside. The choice to build the Citadelle on summits responded to a strategic logic of interior protection which was different from the coastal defense inherited from French colonization.\n\nCreated by a presidential decree in 1978, in order to preserve the splendid natural setting of mountain peaks covered with luxuriant vegetation, the PNH-CSSR covers an area of 25 km2. It includes the group of monuments of the Sans-Souci Palace and its outbuildings, the Citadelle Henry and the Ramiers site, universal symbols of freedom because they are the first structures built by black slaves imported from Africa who conquered their freedom. For Haitians, they represent the most emblematic monuments of their independence.\n\nThese monuments of Haiti, which date back to the beginning of the 19th century, mark the emergence of the Haitian Nation and constitute a coherent whole in their natural environment. They are charged with a universal symbolism that will be worth the inscription of the Natural Historic Park on the World Heritage List by UNESCO. \n\nThey belong to humanity.',
    "plan":"25 km 2 (9.65 square miles)\nAbout 970 m altitude\nEstimated time to visit: at least 4 hours for a visit of the\nmonuments in the Park, from Sans-Souci to the Citadelle.\nAbout 45 to 60 minutes for the hike to the Citadelle.\nFrom Sans-Souci to Choiseul: 4.4 km (2.73 miles)\nFrom Choiseul to Citadel: 1.6 km (1 mile)\nFrom Citadel to Ramiers: 1 km (0.62 mile)",
  
  },
  'fr': {
    'title':'BIENVENUE AU PARC HISTORIQUE NATIONAL DE LA CITADELLE, SANS-SOUCI, RAMIERS',
    'description':"Situé dans la partie nord de la République d'Haïti, le Parc Historique National - Citadelle, Sans Souci, Ramiers (PNH-CSSR) s'inscrit dans la zone centrale des massifs nordiques qui s'étendent jusqu'à la République Dominicaine. La PNH-CSSR est située à la charnière de la plaine, côtière et montagneuse vers l'intérieur. Le choix de construire la Citadelle sur des sommets répondait à une logique stratégique de protection intérieure différente de la défense côtière héritée de la colonisation française.\n\nCréée par décret présidentiel en 1978, afin de préserver le splendide cadre naturel des sommets couverts d'une végétation luxuriante, la PNH-CSSR couvre une superficie de 25 km2. Il comprend l'ensemble des monuments du palais de Sans-Souci et de ses dépendances, la Citadelle Henry et le site des Ramiers, symboles universels de liberté car ce sont les premières structures construites par des esclaves noirs importés d'Afrique qui ont conquis leur liberté. Pour les Haïtiens, ils représentent le plus monuments emblématiques de leur indépendance. \n\nCes monuments d'Haïti, qui remontent au début du XIXe siècle, marquent l'émergence de la Nation haïtienne et constituent un tout cohérent dans leur milieu naturel. Ils sont facturés avec une symbolique universelle qui vaudra l'inscription du Parc historique naturel sur la Liste du patrimoine mondial par l'UNESCO. \n\nIls appartiennent à l'humanité.",
    "plan":"25 km 2 (9,65 miles carrés) \nEnviron 970 m d'altitude \ nDurée de visite estimée: au moins 4 heures pour une visite des \nmonuments du Parc, de Sans-Souci à la Citadelle. \nEnviron 45 à 60 minutes pour le randonnée jusqu'à la Citadelle. \nDe Sans-Souci à Choiseul: 4,4 km (2,73 miles) \nDe Choiseul à Citadelle: 1,6 km (1 mile) \nDe Citadelle à Ramiers: 1 km (0,62 mile)",
   
  },
  'es': {
    'title':'BIENVENIDO AL PARQUE NACIONAL HISTÓRICO CIUDADELA, SANS-SOUCI, EL SITIO DE RAMIERS',
    'description':'Ubicado en la parte norte de la República de Haití, el Parque Nacional Histórico - Ciudadela, Sans-Souci, Ramiers se encuentra dentro de la zona central de los macizos del norte que se extienden hacia la República Dominicana. El PNH-CSSR está ubicado en el cruce de caminos del interior llano, costero y montañoso. La elección de construir la Ciudadela sobre cumbres respondió a una lógica estratégica de protección interior diferente a la defensa costera heredada de la colonización francesa.\n\nCreado por decreto presidencial en 1978, con el fin de preservar el espléndido escenario natural de picos montañosos cubiertos de exuberante vegetación, el PNH-CSSR cubre un área de 25 km2. Abarca el conjunto monumental del Palacio Sans-Souci y sus dependencias, la Ciudadela de Henry y el Site de Ramiers, símbolos universales de la libertad porque son las primeras estructuras construidas por esclavos negros importados de África que conquistaron su libertad. Para los haitianos, representan los monumentos más emblemáticos de su independencia.\n\nEstos monumentos de Haití, que datan de principios del siglo XIX, marcan el surgimiento de la Nación Haitiana y constituyen un todo coherente en su entorno natural. Están cargadas de un simbolismo universal que merecerá la inscripción del Parque Histórico Natural en la Lista del Patrimonio Mundial de la UNESCO. \n\nPertenecen a la humanidad',
    "plan":"25 km 2 \nAproximadamente 970 m de altitud\nTiempo de visita para planificar: al menos 4 horas para una visita monumentos del Parque, desde Sans-Souci hasta la Ciudadela.\nAproximadamente 45 a 60 minutos para la subida a la Ciudadela.\nDistancia entre Sans-Souci y Choiseul: 4,4 km\nDistancia entre Choiseul y Ciudadela: 1,6 km\nDistancia entre la Ciudadela y Ramiers: 1 km"
  }
};

    

List<Map<String, dynamic>> sites = [
  {
    "id":1,
    "name": "name_1",
    "title":"title_1",
    
    "description_bloc_a_1_ht":'Sitadèl Henry \n\nApre 14 ane batay esklav nwa zile a te dirije kont kolon yo, Jean-Jacques Dessalines te pwoklame endepandans Ayiti 1 janvye 1804. Li imedyatman konfye youn nan jeneral li yo, Henri Christophe, swen an. nan bilding sou tèt la Laferrière, yon fò gwo konstriksyon gen entansyon pwoteje jenn repiblik la. \nTou de yon travay militè yo ak yon manifest politik, Sitadèl Henry a ki kanpe nan 970 m anwo nivo lanmè se youn nan pi bon temwen nan atizay la nan militè yo. enjenyè depi nan konmansman an nan 19yèm syèk la. \n\nSitadèl Henry a se fò ki pi enpòtan nan yon rezo ki gen 30 fò ki te bati pou defans Ayiti. Aprè endepandans li nan lane 1804 Jean Jacques Dessalines, ki se lidè prensipal revolisyon an ansanm ak papa endepadans lan, te pase lòd pou bati fò sou tout mòn ki pi wo yo nan peyi a, pou ka pwoteje jèn nasyon an epitou bay Jeneral Henry Christophe misyon pou li konstri sou mòn Laferrière, sou yon wotè ki apeprè 900 mèt konsa, sou Mòn Bonnet rive l’Evêque, gwo fò sa a ki kenbe non Sitadèl Henry. 20,000 moun te patisipe nan konstriksyon li pandan 14 lane.\nSitadèl Henry a se pi gwo depo zam ansyen ki gen nan Karayib la. Kèk nan pyès ki an bwonz yo soti Lafrans, Angletè ak Espay se te yon ekip bèl pyès byen prepare daprè sa atis militè ki te gen nan lane 1700 yo te konn fè. Dis (10) nan yo konsa se sèl an Ayiti yo egziste nan mond lan, yo nan bwat solid ki fèt an bwa yo te vini an toujou.',
    "description_bloc_a_1_fr":'La Citadelle Henry\n\nAprès 14 ans de luttes menées par les esclaves noirs de l’île contre les colons, Jean-Jacques Dessalines proclame l’indépendance d’Haïti le 1er janvier 1804. Il confit immédiatement à l’un de ses généraux, Henri Christophe, le soin de construire sur le pic Laferrière, une gigantesque forteresse destinée à protéger la jeune république.\n A la fois ouvrage militaire et manifeste politique, la Citadelle Henry qui se dresse à 970 m d’altitude est un des meilleurs témoins de l’art des ingénieurs militaires du début du 19ème siècle.\n\nLa Citadelle Henry est la fortification la plus importante d\'un réseau d\'une trentaine de structures de défense construites par Haïti après son indépendance (1804). \nJean-Jacques Dessalines, principal dirigeant de la Révolution et père de l\'indépendance a ordonné de construire des fortifications sur les plus hautes montagnes de au pays pour protéger la jeune République et confie au général Henri Christophe le soin de construire sur le Pic Laferrière, à environ 900 mètres d\'altitude, dans le Bonnet à l\'Evêque, la gigantesque forteresse qui prendra le nom de Citadelle Henry . 20 000 personnes ont participé à sa construction depuis 14 ans. \n\nLa Citadelle Henry est la plus grande batterie des Caraïbes. Certaines des pièces en bronze de France, de Grande-Bretagne et d\'Espagne sont de véritables chefs-d\'œuvre de l\'art militaire du XVIIIe siècle. Une dizaine d\'entre eux, uniques au monde, sont toujours dans leurs voitures d\'origine, en bois massif.',
    "description_bloc_a_1_es":'La Citadelle Henry\n\nDespués de 14 años de luchas dirigidas por los esclavos negros de la isla contra los colonos, Jean-Jacques Dessalines proclamó la independencia de Haití el 1 de enero de 1804. Inmediatamente le confió a uno de sus generales, Henri Christophe, la tarea de construir una gigantesca fortaleza en el Pico de Laferrière para proteger a la joven república. Tanto una obra militar como un manifiesto político, la Ciudadela de Enrique, que se encuentra a 970 m de altitud, es uno de los mejores testigos del arte de los ingenieros militares a principios del siglo XIX".\n\nLa Ciudadela Henry es la fortificación más importante de una red de una treintena de obras de defensas construidas por Haití después de su Independencia (1804).\n Jean Jacques Dessalines, principal líder de la Revolución y padre de la Independencia ordenó construir fortificaciones en las montañas más altas del país para proteger a los jóvenes República y confió al general Henry Christophe el cuidado de construir sobre el Pic Laferrière, a una altitud de unos 900 me- tros, en la cadena de Bonnet à l’Evêque, la gigantesca fortaleza que tomará el nombre de la Ciudadela Henry. 20.000 personas han participó en su construcción durante 14 años.\n Ciudadela Henry es la batería más grande del Caribe. Algunas de las piezas de bronce vienen de Francia, de Gran Bretaña y de España son verdaderas obras maestras del arte militar del siglo XVIII. Aproximadamente diez de ellos, únicos en el mundo, todavía se encuentran en sus carruajes originales de madera maciza.',
    'description_bloc_a_1_en':"The Citadelle Henry\n\nAfter 14 years of struggles led by the island's black slaves against the colonists, Jean-Jacques Dessalines proclaimed Haiti's independence on January 1, 1804. He immediately entrusted one of his generals, Henri Christophe, with the task of building a gigantic fortress on Laferrière Peak to protect the young republic. Both a military work and a political manifesto, the Henry Citadel, which stands at an altitude of 970 m, is one of the best witnesses to the art of military engineers at the beginning of the 19th century.\n\nThe Citadelle Henry is the most important fortification in a network of some 30 defense structures built by Haiti after its Independence (1804).\nJean-Jacques Dessalines, main leader of the Revolution and father of Independence ordered to build fortifications on the highest mountains in the country to protect the young Republic and entrusted General Henri Christophe with the care of build on the Pic Laferrière, at an altitude of around 900 meters, in the Bonnet à l’Evêque, the gigantic fortress which will take the name of the Citadelle Henry. 20,000 people have participated in its construction for 14 years.\n\nThe Citadelle Henry is the largest battery in the Caribbean. Some of the bronze pieces from France, Great Britain and Spain are true masterpieces of art 18th century military. About ten of them, unique to the world, are still in their original carriages, in solid wood.",
    
    "description_bloc_b_1_ht":'\nKi kouvri yon zòn nan sou yon sèl ekta, Sitadèl Henry a te kapab loje yon ganizon nan 2000 a 5000 moun. Okòmansman vin ansent kòm yon moniman pou defans libète kont kolon yo, li kontinye ranfòse apre lanmò Anperè Dessalines, ki te lakòz yon fann nan Repiblik Ayiti: eta sid la, ki te gouvène pa Alexandre Pétion, ak eta nò a, gouvène pa Henri Christophe, ki moun ki Lè sa a, pwoklame tèt li wa, sou non Henry I.',
    "description_bloc_b_1_en":'\nCovering an area of about one hectare, the Citadel Henry could house a garrison of 2000 to 5000 men. Initially conceived as a monument to the defense of liberty against the colonists, it continued to be fortified following the death of Emperor Dessalines, which caused a split in the Republic of Haiti: the southern state, governed by Alexandre Pétion, and the northern state, governed by Henri Christophe, who then proclaimed himself King, under the name of Henry I.',
    'description_bloc_b_1_fr':'\nCouvrant une superficie d’environ un hectare, la Citadelle Henry pouvait abriter une garnison de 2000 à 5000 hommes. Initialement conçue comme un monument à la défense de la liberté contre les colons, elle continua d’être fortifiée suite à la mort de l’empereur Dessalines qui provoqua une scission de la république d’Haïti : l’état du Sud, gouverné par Alexandre Pétion et l’état du Nord, gouverné par Henri Christophe qui s’autoproclame alors Roi, sous le nom d’Henry 1er.',
    'description_bloc_b_1_es':'\nCubriendo un área de aproximadamente una hectárea, la Ciudadela Henry podría albergar una guarnición de 2000 a 5000 hombres. Concebido inicialmente como un monumento a la defensa de la libertad contra los colonos, siguió fortificándose tras la muerte del emperador Dessalines, lo que provocó una escisión en la República de Haití: el Estado del Sur, gobernado por Alexandre Pétion, y el Estado del Norte, gobernado por Henri Christophe, que se proclamó entonces Rey, con el nombre de Enrique I.',
    'description_images':[
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle8.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle9.jpg"
    ],
    "plan_1_en":"\n1. Artillery Museum \n2. Tomb of Prince Noel \n3. The bread oven \n4. Reception desk \n5. Souvenir shop \n6. Refreshment area \n7. Public sanitary facilities \n8. Terrasse du Pont-Levis and the English rooms \n9. The terrace of the Governor's Palace \n10. Albert-Mangonès room",
    "plan_1_fr":"\n1. Musée de l'artillerie\n2. Tombeau du Prince Noël\n3. Le four à pain\n4. Bureau d'accueil\n5. Boutique de souvenirs\n6. Buvette\n7. Sanitaires publics\n8. Terrasse du Pont-Levis et les salles anglaises\n9. La terrasse du palais du Gouverneur\n10. La salle Albert-Mangonès",
    "plan_1_ht":"\n1. Atiri Mize \n2. Kavo Prince Noel \n3. Fou a pen \n4. Biwo Travay Biwo \n5. Boutik souvni \n6. Zòn rafrechisman \n7. Enstalasyon piblik sanitè \n8. Terrasse du Pont-Levis ak chanm angle yo \n9. Teras la nan Palè Gouvènè a \n10. Chan Albert-Mangonès",
    "plan_1_es":"\n1. Museo de Artillería \n2. Tumba del príncipe Noel \n3. El horno de pan \n4. Mostrador de recepción \n5. Tienda de souvenirs \n6. Área de refrescos \n7. Instalaciones sanitarias públicas \n8. Terrasse du Pont-Levis y las habitaciones inglesas \n9. La terraza del Palacio del Gobernador \n10. Sala Albert-Mangonès",


    "info_1_en":"The Citadelle Henry contains a unique collection of artillery pieces of the period. Some bronze pieces from France, Great Britain and Spain are true masterpieces of 18th century military art. Unique in the world, a dozen of them are still in place on their original solid wood mounts.",
    "info_1_fr":"La Citadelle Henry renferme une collection unique au monde des pièces d’artillerie de l’époque. Certaines pièces en bronze venues de France, de Grande-Bretagne et d’Espagne sont de véritables chefs-d’œuvre de l’art militaire du 18ème siècle. Fait unique au monde, une dizaine d’entre-elles sont encore en place sur leurs affûts d’origine en bois massif.",
    "info_1_es":"La Citadelle Henry contiene una colección única de piezas de artillería de la época. Algunas piezas de bronce de Francia, Gran Bretaña y España son verdaderas obras maestras del arte militar del siglo XVIII. Únicos en el mundo, una docena de ellos todavía están en su lugar en sus monturas originales de madera sólida.",
    "info_1_ht":"Citadelle Henry genyen",
    "marker_photo_url":"https://c9hstatic.s3.amazonaws.com/unesco/citadelle_henry.png",
    "image_url":"https://miro.medium.com/max/966/1*ZoM0W2zD0CL_kfNulWUmHA.jpeg",
    "video_url":"https://youtu.be/w8Sh4Qxd2-o",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle5.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle7.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle4.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle1.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/citadelle3.jpg",
    ],
    "lat":19.5735325,
    "lng":-72.2433454,
    "type":"red",
  },
  {
    "id":2,
    "name": "name_2",
    "title":"name_2",
    "description_bloc_a_2_ht":'Palè Sans-Souci a \n\nLi lokalize li tou pre Vilaj Milo a, nan pye wout ki bay aksè nan Sitadel la, Palè San Sousi se eleman prensipal nan yon gran achitekti ki reponn ak tout bezwen pou konsantre rezidans wa ak tout fonksyon nan administrasyon monachi ke Henry Christophe te pwoklame a.\n\nSe te sou rèy Henry I er , Wa Dayiti (1811 - 1820) li te bati. Palè Sansousi a se yon kokennchenn konstriksyon, ki te bay repons a bezwen palè wa a, pifò nan biwo administrasyon wayom tounèf la te ladan l. Konstriksyon an te kòmanse nan lane 1810 epi li te fini nan lane 1813. Li nan mitan yon pakèt mòn, chaje ak bèl pye bwa tout kalite, toutotou li te dekore ak bèl jaden flè, basen ak fontèn dlo. Men lè nou pa te jwenn (Plan, eksplikasyon an detay, foto epòk la) tout detay enfòmasyon yo, li fasil pou imajine liks ak ekstravagans ki t’ap taye banda nan Sansousi nan kòmansman ane 1800 yo. Aspè ki fè palè a pi bèl toujou se fason li fè yon sèl ak bèl espas montay la, ansanm ak divès kalite estil li te prete nan achitekti, yo te panse ki pa te kapab kreye amoni. Lè Wa Henry I mouri 8 Oktòb 1820, yo te piye palè a epi yo te abandone li, sa te kòmanse you pwosesis destriksyon ki te kontinye pandan plis pase 100 lane konsa, ansanm ak endiferans epi yo te bliye l\n\n',
    "description_bloc_a_2_en":'The Palais Sans-Souci\n\nLocated near the village of Milot, at the foot of the access road to the Citadel, the Palais Sans-Souci is the main element of a large architectural complex that responded to the need to concentrate in one place the royal residence and the essential administrative functions of the new monarchy proclaimed by Henry Christophe.\n\n',
    "description_bloc_a_2_fr":'Le Palais Sans-Souci\n\nSitué près du village de Milot, au pied du chemin d’accès à la Citadelle, le Palais Sans-Souci est l’élément principal d’un grand ensemble architectural qui répondait à la nécessité de concentrer dans un même lieu la résidence royale et l’essentiel des fonctions administratives de la nouvelle monarchie proclamée par Henry Christophe.\n\n',
    "description_bloc_a_2_es":'El Palais Sans-Souci\n\nSituado cerca del pueblo de Milot, al pie de la carretera de acceso a la Ciudadela, el Palacio Sans-Souci es el elemento principal de un gran complejo arquitectónico que respondía a la necesidad de concentrar en un solo lugar la residencia real y las funciones administrativas esenciales de la nueva monarquía proclamada por Enrique Cristóbal.\n\n',
    'description_bloc_b_2_ht':"\nAntoure pa masif mòn ki kouvri ak vejetasyon fètil, Palè a ak atnan li yo te gwoupe nan yon sirk sou yon zòn nan sou uit ekta. \Li inogire nan lane 1813, Palè a te piye apre lanmò wa a nan lane 1820. Li te grav domaje nan lane 1842 akoz yon tranbleman tè. Men, li rete solid, gras ak jan li fèt, jan li enpozan ak aderan ruin ki pran bote etranj li yo soti nan yon akò eksepsyonèl ak sit la montay, men tou nan sèvi ak modèl divès ak repitasyon irekonsilyab achitekti.",
    'description_bloc_b_2_en':"\nSurrounded by mountain massifs covered with lush vegetation, the Palace and its outbuildings were grouped in an amphitheater on an area of about eight hectares.\nInaugurated in 1813, the Palace was looted after the King's death in 1820. It was severely damaged by the 1842 earthquake. Nevertheless, it remains, by its dimensions, an imposing and coherent ruin which takes its strange beauty from an exceptional agreement with the mountainous site but also from the use of diverse and reputedly irreconcilable architectural models.",
    'description_bloc_b_2_fr':'\nEntourés de massifs montagneux couverts d’une végétation luxuriante, le Palais et ses dépendances étaient groupés en amphithéâtre sur une superficie d’environ huit hectares.\nInauguré en 1813, le Palais fut pillé à la mort du Roi en 1820. Il fut sévèrement endommagé par le tremblement de terre de 1842.',
    'description_bloc_b_2_es':'\nRodeado por macizos montañosos cubiertos de una exuberante vegetación, el Palacio y sus dependencias se agruparon en un anfiteatro en una superficie de unas ocho hectáreas.\nInaugurado en 1813, el Palacio fue saqueado tras la muerte del Rey en 1820. Fue severamente dañada por el terremoto de 1842. Sin embargo, sus dimensiones la convierten en una ruina imponente y coherente, que debe su extraña belleza a una excepcional armonía con el sitio montañoso y al uso de diversos modelos arquitectónicos considerados irreconciliables.',
    'description_images':[
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci2.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci5.jpg"
    ],    

    "plan_2_en":"",
    "plan_2_fr":"",
    "plan_2_ht":"",
    "plan_2_es":"",

    "info_2_ht":"Stil nan gwo kay sa a, rele byen \"Vèsay Karayib yo\" pa temwen yo nan tan an, se rezilta nan yon melanj enspire pa tout bagay ki te bati nan 18tyèm syèk la nan barok Ewòp, ki gen ladan Palè a, wa a nan Prussia men tou palè Lond yo. Anbisyon Henry Christophe la se te demontre devan je loksidan yo pa jès sa a ke desandan Afriken yo pa te pèdi «gou achitekti ak jeni zansèt yo ki te kouvri Etyopi, Ejip, Carthage ak ansyen Espay ak moniman sipèb yo»",
    "info_2_en":"The style of this building, quickly nicknamed the « Versailles des Caraïbes » by the witnesses of the time, is the result of a mixture inspired by everything that was built in the 18th century in Baroque Europe, including the Palace of the King of Prussia but also the London palaces. Henry Christophe's ambition was to demonstrate to the eyes of Westerners by this gesture that the descendants of Africans had not lost the « architectural taste and genius of their ancestors who covered Ethiopia, Egypt, Carthage and ancient Spain with their superb monuments »",
    "info_2_fr":"Le style de cet édifice surnommé rapidement par les témoins de l'époque le « Versailles des Caraïbes », est le fruit d'un mélange inspiré de tout ce qui s'était construit au XVIIIe siècle dans l'Europe baroque, dont le Palais du roi de Prusse mais aussi les palais londoniens. Henry Christophe avait pour ambition de démontrer aux yeux des Occidentaux par ce geste que les descendants d'Africains n'avaient pas perdu le « goût architectural et le génie de leurs ancêtres qui ont couvert l'Éthiopie, l'Égypte, Carthage et l'ancienne Espagne avec leurs superbes monuments »",
    "info_2_es":"El estilo de este edificio, rápidamente apodado el « Versalles del Caribe » por los testigos de la época, es el resultado de una mezcla inspirada en todo lo que se construyó en el siglo XVIII en la Europa barroca, incluyendo el Palacio del Rey de Prusia pero también los palacios de Londres. La ambición de Henry Christophe era demostrar a los ojos de los occidentales con este gesto que los descendientes de africanos no habían perdido el « gusto arquitectónico y el genio de sus antepasados que cubrieron Etiopía, Egipto, Cartago y la antigua España con sus magníficos monumentos »",
    "marker_photo_url":"https://c9hstatic.s3.amazonaws.com/unesco/sans_souci.png",
    "image_url":"",
    "video_url":"https://youtu.be/UB_jsCU2OhY",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci6.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci3.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci4.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/sans-souci5.jpg",
    ],
    "lat":19.6047122,
    "lng": -72.2211336,
    "type":"red",
  },
  {
    "id":3,
    "name": "name_3",
    "title":"title_3",
    "description_bloc_a_3_ht":"li ranfòse sit Ramiers \n\nSit Ramiers se yon ti plato, ki chita sou sid sid Sitadèl la, ki kòmande yon panorama sipèb epi revele yon aspè inatandi nan fò a mayifik. Li posib ke yon enjenyè militè eklere nan epòk la idantifye yon wout aksè sekrè ki ta ka mennen nan pye miray Sitadèl la pou twoup lènmi posib yo, ki te mennen nan konstriksyon yon estrikti defans adisyonèl. Li difisil pou konnen si travay sa a te antreprann le pli vit ke Sitadèl la te louvri oswa si li te yon refleksyon pita ki devwale pwen sa a fèb. Nan nenpòt ka, kat redout yo te fèt pou defann kat flann Sitadèl la. \n",
    "description_bloc_a_3_en":"The fortified site of Ramiers\n\nThe site of Ramiers is a small plateau, located on the southern flank of the Citadel, which commands a superb panorama and reveals an unexpected aspect of the grandiose fortress. It is likely that an enlightened military engineer of the time identified a discreet access route that could lead to the foot of the Citadel's walls for possible enemy troops, which led to the construction of an additional defensive structure. It is difficult to know if this work was undertaken as soon as the Citadel was opened or if it was a later reflection that revealed this weak point. In any case, the four redoubts were designed to defend the four flanks of the Citadel.\n",
    "description_bloc_a_3_fr":'Le site fortifié des Ramiers\n\nLe site des Ramiers est un petit plateau, situé sur le flanc Sud de la Citadelle, qui commande un superbe panorama et révèle un aspect inattendu de la grandiose forteresse. Il est probable qu’un ingénieur militaire éclairé de l’époque ait identifié une voie d’accès discrète pouvant menée au pied des murailles de la Citadelle pour d’éventuelles troupes ennemies, ce qui amena à l’édification d’un ouvrage supplétif de défense. Il est difficile de savoir si cet ouvrage a été entrepris dès l’ouverture du chantier de la Citadelle ou si c’est une réflexion ultérieure qui révéla ce point faible. Quoiqu’il en soit les quatre redoutes ont été conçues pour défendre les quatre flancs de la Citadelle.\n',
    "description_bloc_a_3_es":'El sitio fortificado de Les Ramiers\n\nEl sitio de Les Ramiers es una pequeña meseta, situada en el flanco sur de la Ciudadela, que domina un magnífico panorama y revela un aspecto inesperado de la grandiosa fortaleza. Es probable que un ingeniero militar ilustrado de la época identificara una ruta de acceso discreta que pudiera conducir al pie de las murallas de la Ciudadela para posibles tropas enemigas, lo que llevó a la construcción de una obra defensiva adicional. Es difícil saber si este trabajo se llevó a cabo tan pronto como se abrió el sitio de construcción de la Ciudadela o si fue una reflexión posterior la que reveló este punto débil. En cualquier caso, los cuatro reductos fueron diseñados para defender los cuatro flancos de la Ciudadela.\n',
    'description_bloc_b_3_ht':'\nBaze sou zam yo dekouvri anndan redout Ramiers yo ansanm ak layout yo sou sit la, de ipotèz te kapab ekspoze pa ekspè yo konsènan misyon yo: yon misyon siveyans pa mwayen vijilan sou wotè redout yo, veritab vijilatè ak yon View pwolonje sou perimèt la tout antye de plato a ak flann sid Sitadèl la; ak yon misyon defans gras a moso ti kalib ki te ame yo: kanon, obu, ak zam pòtab.',
    'description_bloc_b_3_en':'\nBased on the weaponry discovered inside the Ramiers redoubts as well as their layout on the site, two hypotheses could be put forward by the experts as to their missions: a surveillance mission by means of lookouts on the heights of the redoubts, veritable lookouts with a view extending over the entire perimeter of the plateau and the southern flank of the Citadel; and a defense mission thanks to the small-caliber pieces that armed them: cannons, howitzers, and portable firearms.',
    'description_bloc_b_3_fr':"\nEn s’appuyant sur l’armement découvert à l’intérieur des redoutes de Ramiers ainsi que sur leur disposition sur le site, deux hypothèses ont pu être avancées par les experts quant à leurs missions :\n\n° une mission de surveillance par le biais de guetteurs sur les hauteurs des redoutes, véritables vigies avec une vue s’étendant sur tout le périmètre du plateau et du flanc sud de la Citadelle\n° une mission de défense grâce aux pièces de petit calibre qui les armaient : des canons, des obusiers, et des armes à feu portatives.",
    'description_bloc_b_3_es':'\nBasándose en el armamento descubierto en el interior de los reductos de Ramiers, así como en su disposición en el lugar, los expertos plantearon dos hipótesis en cuanto a sus misiones: una misión de vigilancia mediante miradores en las alturas de los reductos, verdaderos miradores con una vista que se extiende por todo el perímetro de la meseta y el flanco sur de la Ciudadela; y una misión de defensa gracias a las armas de pequeño calibre que los armaban: cañones, obuses y armas de fuego portátiles.',
    "description_images":[
        "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/Vue frontale d'une redoute à Ramiers-min.JPG",
        "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/Vue laterale de l'une des redoutes des ramiers-min.JPG"
    ],
    "plan_3_en":"",
    "plan_3_fr":"",
    "plan_3_ht":"",
    "plan_3_es":"",

    "info_3_ht":"Nan sant sa a ti plato nan Ramiers yo kraze yo trè domaje nan sa ki te kapab yon enpòtan, petèt menm rezidans wa yo. Li sipoze ke rezidans sa a pita te vin fè pati yon zòn ki pwoteje pou akomode kout sejou pou plezi Marie-Louise Coidavid, madanm wa Henry I.",
    "info_3_en":"In the center of this small plateau of Ramiers are the very damaged ruins of what could have been an important, perhaps even royal residence. It is assumed that this residence later became part of a protected area to accommodate short stays for the pleasure of Marie-Louise Coidavid, wife of King Henry I.",
    "info_3_fr":"Au centre de ce petit plateau des Ramiers, se trouvent les ruines très abimées de ce qui aurait pu être une résidence importante, peut-être même royale. On suppose que cette résidence est venue s’inscrire ensuite dans un périmètre protégé pour accueillir de courts séjours d’agrément de Marie-Louise Coidavid, épouse du roi Henry 1er.",
    "info_3_es":"En el centro de esta pequeña meseta de Ramiers están las ruinas muy dañadas de lo que podría haber sido una importante, quizás incluso una residencia real. Se supone que esta residencia se convirtió más tarde en parte de un perímetro protegido para acomodar estancias cortas para el placer de María Luisa Coidavid, esposa del Rey Enrique I.",
    "marker_photo_url":"https://c9hstatic.s3.amazonaws.com/unesco/ramiers.png",
    "image_url":"",
    "video_url":"",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/Ramiers.Raphaelle Castera.Ispan.2011-min.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/Vestige du palais de la Reine à  Ramiers-min.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/patrimoine/Vestige d’une redoute à Ramiers-min.JPG",
            
    ],
    "lat":19.564712,
    "lng":-72.242371,
    "type":"red",
  },

  {
    "id":4,
    "name": "name_4",
    "title":"title_4",
    "description_bloc_4_en":'english',
    'description_bloc_4_fr':'french',
    'description_bloc_4_es':'spanish',
    'description_bloc_4_ht':'creole',
    "info":"",
     "plan_4_en":"",
    "plan_4_fr":"",
    "plan_4_ht":"",
    "plan_4_es":"",

    "marker_photo_url":"",
    "image_url":"https://upload.wikimedia.org/wikipedia/commons/2/23/Sans-Souci_Palace%2C_National_History_Park%2C_Haiti.jpg",
    "video_url":"",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/infospratiques/pic1.jpeg",
      "https://c9hstatic.s3.amazonaws.com/unesco/infospratiques/pic2.jpeg",
    ],
    "lat":19.607062,
    "lng": -72.216548,
    "type":"purple",
  },

  {
    "id":5,
    "name": "name_5",
    "title":"title_5",
    "description_bloc_5_ht":'Lis fòn avyè yo nan pak la ak zòn pezib li yo gen ladan 66 espès, divize an 15 endemik, 5 prezante, 11 migratè ak 35 espès rezidan. Pami espès sa yo, gen 5 ki nan lis wonn IUCN nan espès ki menase yo.',
    "description_bloc_5_en":'The list of avian fauna in the Park and its buffer zones includes 66 species, divided into 15 endemic, 5 introduced, 11 migratory and 35 resident species. Among these species there are 5 that are listed on the IUCN Red List of Threatened Species. ',
    'description_bloc_5_fr':'La liste de la faune aviaire du Parc et de ses zones tampons compte 66 espèces, réparties en 15 endémiques, 5 introduites, 11 migratrices et 35 résidents. Parmi ces espèces il y a 5 qui sont Listées sur la liste rouge de l’UICN ',
    'description_bloc_5_es':'La lista de la fauna aviar del Parque y sus zonas de amortiguación incluye 66 especies, divididas en 15 endémicas, 5 introducidas, 11 migratorias y 35 residentes. De estas especies, cinco están incluidas en la Lista Roja de Especies Amenazadas de la UICN.',
    "info":"",
    "marker_photo_url":"",
    "image_url":"",
    "video_url":"",
     "plan_5_en":"",
    "plan_5_fr":"",
    "plan_5_ht":"",
    "plan_5_es":"",

    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic1.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic5.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic20.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic18.jpg",
    ],
    "lat":19.598206,
    "lng": -72.209104,
    "type":"green",
    "content_images":[
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/avifaunes/1.jpg",
        "url":"Vireo altiloquus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/avifaunes/2.jpg",
        "url":"Melanerpes striatus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/avifaunes/3.jpg",
        "url":"Cathartes aura"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/avifaunes/4.jpg",
        "url":"Nesoctites micromegas"
      }
    ]
  },

  {
    "id":6,
    "name": "name_6",
    "title":"title_6",
    "description_bloc_6_ht":'Pami 13 reptil yo anrejistre yo se 2 entwodwi ak 11 espès andemik. Okenn nan espès sa yo pa sou Wouj Lis Wouj la ak yon nivo nan menas enkyetid. Nan lòt men an, distribisyon an ak kantite moun ki anrejistre pou chak nan espès sa yo nan HNP-CSSR a trè ba.',
    "description_bloc_6_en":'Among the 13 reptiles recorded are 2 introduced and 11 endemic species. None of these species are on the IUCN Red List with a level of threat of concern. On the other hand, the distribution and number of individuals recorded for each of these species within the HNP-CSSR are very low.',
    'description_bloc_6_fr':'Parmi les 13 reptiles recensés on compte 2 espèces introduites et 11 endémiques. Aucune de ces espèces figurent sur la liste rouge de l’UICN avec un niveau de menace préoccupante. Par contre, la distribution et le nombre d’individus recensés pour chacune de ces espèces au sein du PNH-CSSR sont très faibles.',
    'description_bloc_6_es':'Entre los 13 reptiles registrados hay 2 introducidos y 11 especies endémicas. Ninguna de estas especies está en la Lista Roja de la UICN con un nivel de amenaza de especial preocupación. Por otro lado, la distribución y el número de individuos registrados para cada una de estas especies dentro del HNP-CSSR son muy bajos.',
    "info":"",
    "marker_photo_url":"",
    "image_url":"",
    "video_url":"",
     "plan_6_en":"",
    "plan_6_fr":"",
    "plan_6_ht":"",
    "plan_6_es":"",

    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic2.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic4.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic6.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic9.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic10.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic15.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic19.jpg",
    ],
    "lat":19.540574,
    "lng": -72.236809,
    "type":"green",
    "content_images":[
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/reptiles/1.jpg",
        "url":"Anolis distichus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/reptiles/2.jpg",
        "url":"Chilabotrus striatus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/reptiles/3.jpg",
        "url":"Anolis chlorocianus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/reptiles/4.jpg",
        "url":"Anolis hispaniolae"
      }
    ]
  },

  {
    "id":7,
    "name": "name_7",
    "title":"title_7",
    "description_bloc_7_ht":"Klas sa a nan 25 espès gen ladan 2 entwodwi ak 5 espès andemik. Gen 3 espès ki prezan nan tout sit. Se yon sèl detekte sèlman nan sit la nan Dondon, nan lokalite a nan Brostage.",
    "description_bloc_7_en":'This class of 25 species includes 2 introduced and 5 endemic species. There are 3 species that are present in all sites. Only one is detected only in the site of Dondon, in the locality of Brostage. ',
    'description_bloc_7_fr':'Cette classe de 25 espèces en comporte 2 introduites et 5 endémiques. Il y a 3 espèces qui sont présentes dans tous les sites. Une seule est décelée uniquement dans le site de Dondon, dans la localité de Brostage.',
    'description_bloc_7_es':'Esta clase de 25 especies incluye 2 introducidas y 5 endémicas. Hay 3 especies que están presentes en todos los sitios. Sólo se encuentra uno en el sitio de Dondon, en la localidad de Brostage.',
    "info":"",
    "marker_photo_url":"",
    "image_url":"",
    "video_url":"",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic3.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic8.jpg",
    ],
     "plan_7_en":"",
    "plan_7_fr":"",
    "plan_7_ht":"",
    "plan_7_es":"",

    "lat":19.561,
    "lng": -72.228,
    "type":"green",
    "content_images":[
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/amphibiens/1.jpg",
        "url":"Eleutherodactylus abotti"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/amphibiens/2.jpg",
        "url":"Rhinella marina"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/amphibiens/3.jpg",
        "url":"Dominicensis"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/amphibiens/4.jpg",
        "url":"Peltophryne guentheri"
      }
    ]
  },

  {
    "id":8,
    "name": "name_8",
    "title":"title_8",
    "description_bloc_8_ht":'Karakterize pa posesyon yon kòf espiral ak kat gwo zèl plis oswa mwens kal. Pi remakab nan atropod yo se papiyon. Selon yon estimasyon resan, gen 1180 espès lepidoptera sou Hispaniola.',
    "description_bloc_8_en":'Characterized by the possession of a spiral trunk and four large wings more or less scaly. The most remarkable of the arthropods are butterflies. According to a recent estimate, there are 1180 species of lepidoptera on Hispaniola.',
    'description_bloc_8_fr':'Caractérisée par la possession d’une trompe en spirale et de quatre grosses ailes plus ou moins écailleuses. Les plus remarquables des arthropodes sont les papillons. Selon une estimation récente, il y a 1180 espèces de lépidoptères sur Hispaniola.',
    'description_bloc_8_es':'Se caracteriza por la posesión de un tronco espiral y cuatro grandes alas más o menos escamosas. Los artrópodos más notables son las mariposas. Según una estimación reciente, hay 1180 especies de lepidópteros en la Hispaniola.',
    "info":"",
    "marker_photo_url":"",
    "image_url":"",
    "video_url":"",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic7.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic16.jpg",
    ],
     "plan_8_en":"",
    "plan_8_fr":"",
    "plan_8_ht":"",
    "plan_8_es":"",

    "lat":19.591182,
    "lng": -72.229453,
    "type":"green",
    "content_images":[
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/lepidopteres/1.jpg",
        "url":"Urbanus proteus"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/lepidopteres/2.jpg",
        "url":"Battus polydamas"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/lepidopteres/3.jpg",
        "url":"Historis odius"
      },
      {
        "info":"https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/lepidopteres/4.jpg",
        "url":"Tiger mimic"
      }
    ]
  },
  {
    "id":9,
    "name": "name_9",
    "title":"title_9",
    "description_bloc_9_ht":"Lòd Odonates yo gen ladan demwazèl yo ak jenn fi yo. Predatè koripsyon, yo kontwole popilasyon ensèk kote yo manje. Yo souvan abita akwatik sitou pou repwodiksyon. Selon dènye etid yo, gen 67 espès odonat nan Hispaniola, 7 ladan yo endemik.",
    "description_bloc_9_es":'La Orden de los Odonatos incluye a las libélulas y a las damas. Voraces depredadores, controlan las poblaciones de insectos de las que se alimentan. Frecuentan los hábitats acuáticos principalmente para la reproducción. Según estudios recientes, hay 67 especies de odonatos en la Hispaniola, 7 de las cuales son endémicas.',
    'description_bloc_9_fr':'L’Ordre des Odonates regroupe les Libellules et les Demoiselles. Prédateurs voraces, ils contrôlent les populations d’insecte dont ils se nourrissent. Ils fréquentent les habitats aquatiques essentiellement pour la reproduction. Selon les études récentes, il y a 67 espèces d’odonates à Hispaniola dont 7 endémiques.',
    'description_bloc_9_en':'The Order of the Odonates includes the Dragonflies and the Maidens. Voracious predators, they control the insect populations on which they feed. They frequent aquatic habitats mainly for reproduction. According to recent studies, there are 67 species of odonates in Hispaniola, 7 of which are endemic.',
    "info":"",
     "plan_9_en":"",
    "plan_9_fr":"",
    "plan_9_ht":"",
    "plan_9_es":"",

    "marker_photo_url":"",
    "image_url":"",
    "video_url":"",
    "images":[
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic12.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic13.jpg",
      "https://c9hstatic.s3.amazonaws.com/unesco/biodiversite/pic14.jpg",
    ],
    "lat":19.549354,
    "lng": -72.217711,
    "type":"green",
    "content_images":[
    ]
  },
];