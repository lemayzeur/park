import 'package:flutter/material.dart';

const primaryColorDark = const Color(0xffec1d24);
const primaryColor = const Color(0xfff68e92);
const secondaryColor = const Color(0xff0c4da2);
const secondaryColorDark = const Color(0xff041e40);
const bgColor = const Color(0xffFBCD3F);
const textColor = const Color(0xffFBCD3F);