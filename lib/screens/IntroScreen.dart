import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:park/colors.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:park/utils.dart';

class IntroScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: secondaryColorDark,
          title: Text("intro_title", style: TextStyle(color: textColor)).tr(),
          automaticallyImplyLeading: true,
          leading: IconButton(icon: Icon(Icons.arrow_back,color: textColor),
            onPressed: (){Navigator.pop(context, false);}
          ),
        ),
        body: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            child: Column(
              children:[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  child: Image (
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/bg.png'),
                  ),
                ),
                Container(
                  color:bgColor,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: ListTile(
                        title: Text(intros[context.locale.languageCode]['title'], 
                        style: TextStyle(fontSize:15.0,
                        fontWeight: FontWeight.bold)),
                      ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal:10.0,vertical: 10.0),
                  child: RichText(
                    textAlign: TextAlign.justify,
                    text:TextSpan(text: intros[context.locale.languageCode]['description'],
                      style: TextStyle(fontSize:16.0, color:Colors.black)
                    )
                  ),
                ),
                Container(
                  color:bgColor,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: ListTile(
                        title: Text("visit_title", style: TextStyle(fontSize:15.0,fontWeight: FontWeight.bold)).tr(),
                      ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal:10.0,vertical: 10.0),
                  child: RichText(
                    textAlign: TextAlign.justify,
                    text:TextSpan(text: intros[context.locale.languageCode]['plan'],
                      style: TextStyle(fontSize:16.0, color:Colors.black)
                    )
                  ),
                ),
              ]
            ),
          ),
        )
      ),
    );
  }
}