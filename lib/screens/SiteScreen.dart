import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:park/colors.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:park/screens/ImageViewScreen.dart';
import 'package:responsive_grid/responsive_grid.dart';

class SiteScreen extends StatefulWidget {
  final dynamic site;

  const SiteScreen({ Key key , this.site}) : super(key: key);

  @override
  _SiteScreenState createState() => _SiteScreenState();
}

class _SiteScreenState extends State<SiteScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  YoutubePlayerController _controller;
  // PlayerState _playerState;
  // YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;
  // int _current = 0;

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        // _playerState = _controller.value.playerState;
        // _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = widget.site['video_url'] != null && widget.site['video_url'].isNotEmpty ? YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(widget.site['video_url']),
        flags: YoutubePlayerFlags(
            autoPlay: false,
            mute: false,
            loop: false,
            isLive: false,
            forceHD: false,
            enableCaption: true,
        ),
    ) : null;
    List allImages = widget.site['images'];
    String mainImageUrl = widget.site["image_url"];
    List contentImages = widget.site['content_images'];
    List descriptionImages = widget.site['description_images'];
    if(mainImageUrl.isNotEmpty)
      allImages.add(mainImageUrl);
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: secondaryColorDark,
          title: Text(widget.site['name'], style:TextStyle(color:textColor)).tr(),
          automaticallyImplyLeading: true,
          leading: IconButton(icon: Icon(Icons.arrow_back, color: textColor,),
            onPressed: (){Navigator.pop(context, false);}
          ),
        ),
        body: SingleChildScrollView(
          // padding: EdgeInsets.all(10.0),
          physics: AlwaysScrollableScrollPhysics(),
          child:Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            children:<Widget>[
              
              CarouselSlider(
                options: CarouselOptions(
                  autoPlayInterval: Duration(seconds: 12),
                  autoPlayAnimationDuration: Duration(milliseconds: 1000),
                  enableInfiniteScroll: false,
                  enlargeCenterPage: false,
                  autoPlay: true,
                  viewportFraction: 1.0,
                  onPageChanged: (index, reason) {
                    
                  }
                ),
                items: allImages.map((imageUrl) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Color(0xFFEEEEEE),
                        ),
                        child:CachedNetworkImage(
                          progressIndicatorBuilder: (context, url, progress) =>
                              Center(
                                child: Container(
                                  height:45.0, width:45.0,
                                  child: CircularProgressIndicator(
                                    value: progress.progress,
                                  ),
                                ),
                              ),
                          imageUrl: imageUrl,
                          errorWidget: (context, url, error) => const Icon(Icons.error),
                          fadeOutDuration: const Duration(seconds: 1),
                          fadeInDuration: const Duration(seconds: 2),
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
              Container(
                color:bgColor,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: ListTile(
                      // leading: Icon(Icons.bookmark_border, color: Colors.orange[600]),
                      leading: Image.asset("assets/images/title_icon.png",width:28, height:28),
                      title: Text(widget.site['name'], 
                        style: TextStyle(fontSize:25.0, fontWeight: FontWeight.bold)).tr(),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal:10.0),
                child:Column(
                  children: <Widget>[
                    
                    SizedBox(height:12.0),
                    widget.site["id"] == 4 ? 
                    Column(children:<Widget>[
                      ListTile(
                        leading: Icon(Icons.airplanemode_active),
                        title: Text('road_from_airport').tr(),
                        subtitle: Text("entry_fee").tr(),
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Icon(Icons.time_to_leave, color:Colors.black38),
                              Text("parking").tr(),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Icon(Icons.delete_sweep, color:Colors.black38),
                              Text("toilet").tr(),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Icon(Icons.store, color:Colors.black38),
                              Text("store").tr(),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Icon(Icons.directions_walk, color:Colors.black38),
                              Text("guide").tr(),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Icon(Icons.restaurant, color:Colors.black38),
                              Text("restaurant").tr(),
                            ],
                          ),
                        ],
                      ),
                      Divider(),
                      SizedBox(height:12.0),
                      Text("access_to", style: TextStyle(fontWeight:FontWeight.bold)).tr(),
                      ListTile(
                        title: Text('way_1').tr(),
                        subtitle: Text("time_1").tr(),
                      ),
                      ListTile(
                        title: Text('way_2').tr(),
                        subtitle: Text("time_2").tr(),
                      ),
                      Divider(),
                      ListTile(
                        title: Text('visit_text').tr(),
                        subtitle: Text("visit_duration").tr(),
                      ),
                    ]) : widget.site["id"] <= 3 ? 
                    Column(
                      children:<Widget>[
                        RichText(
                          textAlign: TextAlign.justify,
                          text:TextSpan(text:widget.site["description_bloc_a_${widget.site["id"]}_${context.locale.languageCode}"],
                            style: TextStyle(fontSize:16.0, color:Colors.black)
                          )
                        ),
                        Row(
                          children:descriptionImages.map((e){
                            return Expanded(
                              flex:5,
                              child:CachedNetworkImage(
                                progressIndicatorBuilder: (context, url, progress) =>
                                Center(
                                  child: Container(
                                    height:25.0, width:25.0,
                                    child: CircularProgressIndicator(
                                      value: progress.progress,
                                    ),
                                  ),
                                ),
                                imageUrl: e,
                                errorWidget: (context, url, error) => const Icon(Icons.error),
                                fit: BoxFit.cover,
                              ),
                            );
                          }).toList(),
                        ),
                        RichText(
                          textAlign: TextAlign.justify,
                          text:TextSpan(text:widget.site["description_bloc_b_${widget.site["id"]}_${context.locale.languageCode}"],
                          style: TextStyle(fontSize:16.0, color:Colors.black))
                        )
                      ]
                    ) : RichText(textAlign: TextAlign.justify,text:TextSpan(text:widget.site["description_bloc_${widget.site["id"]}_${context.locale.languageCode}"],
                          style: TextStyle(fontSize:16.0, color:Colors.black))),
                  ]
                )
              ),

              widget.site['info_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? SizedBox(height:20.0) : Container(),
              widget.site['info_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? Container(
                color:bgColor,
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: ListTile(
                      title: Text("plan_visit", style: TextStyle(fontSize:25.0,
                      fontWeight: FontWeight.bold)).tr(),
                    ),
                ),
              ): Container(),

              widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: GestureDetector(
                  child: Image (
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/map.png'),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => ImageViewScreen(imageUrl:'assets/images/map.png', info: ("plan_visit").tr(),isOnline:false)));
                  },
                ),
              ) : Container(),


              widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['plan_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? Container(
                padding: EdgeInsets.symmetric(horizontal:10.0),
                alignment: Alignment.centerLeft,
                child:Column(
                  children: <Widget>[
                    SizedBox(height:12.0),
                    Text('legend_title', 
                      style: TextStyle(
                        fontWeight:FontWeight.bold,
                        fontSize:18.0,
                        ),
                        textAlign: TextAlign.left
                      ).tr(),
                    SizedBox(height:12.0),
                    RichText(textAlign: TextAlign.justify,text:TextSpan(text:widget.site["plan_${widget.site['id']}_${context.locale.languageCode}"],style: TextStyle(fontSize:16.0, color:Colors.black)))
                  ]
                )
              ) : Container(),


              widget.site['info_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['info_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? SizedBox(height:20.0) : Container(),
              widget.site['info_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['info_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? Container(
                color:bgColor,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: ListTile(
                      leading: Icon(Icons.lightbulb_outline, color: secondaryColorDark),
                      title: Text("you_know", style: TextStyle(fontSize:25.0,
                      fontWeight: FontWeight.bold)).tr(),
                    ),
                ),
              ): Container(),
              Container(
                padding: EdgeInsets.symmetric(horizontal:10.0),
                child:Column(
                  children: <Widget>[
                    SizedBox(height:12.0),
                    widget.site['info_${widget.site['id']}_${context.locale.languageCode}'] != null && widget.site['info_${widget.site['id']}_${context.locale.languageCode}'].length > 1 ? 
                      RichText(textAlign: TextAlign.justify,text:TextSpan(text:widget.site["info_${widget.site['id']}_${context.locale.languageCode}"],style: TextStyle(fontSize:16.0, color:Colors.black))) :
                      Container(),
                    widget.site['video_url'] != null && widget.site['video_url'].isNotEmpty ? Divider(height:12.0) : Container(),
                    widget.site['video_url'] != null && widget.site['video_url'].isNotEmpty ? YoutubePlayer(
                        controller: _controller,
                        showVideoProgressIndicator: true,
                        progressIndicatorColor: Colors.amber,
                        progressColors: ProgressBarColors(
                            playedColor: Colors.amber,
                            handleColor: Colors.amberAccent,
                        ),
                        onReady: (){
                            _controller.addListener(listener);
                        },
                        
                    ) : Container(),
                    
                  ]
                )
              ),
              SizedBox(height:12.0),
              contentImages != null && contentImages.length > 0 ? Container(
                // color: Colors.black54,
                child:Text("© Unesco/René Durocher", style: TextStyle(color: Colors.black38))
              ) : Container(),
              SizedBox(height:6.0),
              contentImages != null && contentImages.length > 0 ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: ResponsiveGridRow(
                  children:contentImages.map<ResponsiveGridCol>((bloc){
                    return ResponsiveGridCol(
                      xs:6,
                      md:6,
                      lg:6,
                      child:Container(
                        padding: EdgeInsets.symmetric(horizontal:6.0, vertical:6.0),
                        child: Column(
                          children: <Widget>[
                            Wrap(
                              children:[
                                GestureDetector(
                                  child: CachedNetworkImage(
                                    progressIndicatorBuilder: (context, url, progress) =>
                                        Center(
                                          child: Container(
                                            height:45.0, width:45.0,
                                            child: CircularProgressIndicator(
                                              value: progress.progress,
                                            ),
                                          ),
                                        ),
                                    imageUrl: bloc['info'],
                                    errorWidget: (context, url, error) => const Icon(Icons.error),
                                    fadeOutDuration: const Duration(seconds: 1),
                                    fadeInDuration: const Duration(seconds: 2),
                                    fit: BoxFit.cover,
                                  ),
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => ImageViewScreen(imageUrl:bloc['info'], info: bloc['url'],isOnline:true)));
                                  }
                                ),
                                SizedBox(height:12.0),
                                Container(
                                  // color: Colors.black54,
                                  child:Text(bloc['url'], style: TextStyle(fontSize:16.0))
                                ),
                                SizedBox(height:12.0),
                              ]
                            ),
                          ],
                        ),
                      )
                    );
                  }).toList(), 
                ),
              ) : Container()
            ]
          )
        ),
      ),
    );
  }
}