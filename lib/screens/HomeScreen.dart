import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:park/colors.dart';
import 'package:park/screens/IntroScreen.dart';
import 'package:park/screens/MapScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 25.0),
            child: Image(
              image: AssetImage("assets/images/top.png"),
              fit: BoxFit.fitWidth,
            ),
          ),
          backgroundColor: bgColor,
          leading: IconButton(icon: Icon(Icons.arrow_back, color: secondaryColorDark),
            onPressed: (){Navigator.pop(context, false);}
          ),
          actions : <Widget>[
            FlatButton(
              child: Text("intro").tr(),
              onPressed: (){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => IntroScreen()));
              }
            ),
          ]
        ),
        body: SizedBox.expand(
          child: Column(
            children: <Widget>[
              Container(
                color: bgColor,
                child: Row(
                  children: <Widget>[
                    // Container(
                    //   width: MediaQuery.of(context).size.width,
                    //   height: 200,
                    //   child: Image (
                    //     fit: BoxFit.fill,
                    //     image: AssetImage('assets/images/logo.png'),
                    //   ),
                    // ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 200,
                          child: Image (
                            fit: BoxFit.fill,
                            image: context.locale.languageCode == 'fr' || context.locale.languageCode == 'es' ? AssetImage('assets/images/logo.png') : AssetImage('assets/images/logo_ht.png'),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        RaisedButton(
                          // splashColor: Colors.red,
                          color: Colors.orangeAccent,
                          child: Text('discovery_button',
                            style: new TextStyle(
                              fontSize: 16.0,
                              color: secondaryColorDark, 
                            ),
                          ).tr(),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) => MapScreen()));
                          },
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 280.5,
                          child: Image (
                            fit: BoxFit.fill,
                            image: AssetImage('assets/images/citadelle.jpg'),
                          ),
                        ),
                      ],
                    ),
                    // Container(
                    //   child: ,
                    // )
                    // Stack(children: <Widget>[
                    //   Align(
                    //     alignment: Alignment.bottomCenter,
                    //     child: Text("Hello")
                    //   ),
                    // ]),
                // child: Image (
                //   fit: BoxFit.fitHeight,
                //   image: AssetImage('assets/myimage.png'),
                // ),
                  ]
                )
              ),
            ],
          ),
        ),
      
        
      ),
    );
  }
}