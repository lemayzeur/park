import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:park/colors.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class InfoScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: secondaryColorDark,
          title: Text("info_title", style:TextStyle(color: textColor)).tr(),
          automaticallyImplyLeading: true,
          leading: IconButton(icon: Icon(Icons.arrow_back,color: textColor,),
            onPressed: (){Navigator.pop(context, false);}
          ),
        ),
        body: Container(
          child:ListView(
            children: <Widget>[
              Card(
                child: ListTile(
                  leading: FaIcon(FontAwesomeIcons.facebook, color: Color(0xFF3b5998)),
                  title: Text('info_facebook').tr(),
                  onTap: (){
                    launch("https://facebook.com/Pak.Nasyonal.Istorik");
                  }
                ),
              ),
              Card(
                child: ListTile(
                  leading: FaIcon(FontAwesomeIcons.twitter, color: Color(0xFF00acee)),
                  title: Text('info_twitter').tr(),
                  onTap: (){
                    launch("https://twitter.com/Pnh_CSSR");
                  },
                ),
              ),
              Card(
                child: ListTile(
                  leading: FaIcon(FontAwesomeIcons.instagram, color: Color(0xFF8a3ab9)),
                  title: Text('info_instagram').tr(),
                  onTap: (){
                    launch("https://instagram.com/parc_national_historique_haiti");
                  },
                ),
              ),
              Card(
                child: ListTile(
                  leading: FaIcon(FontAwesomeIcons.globe),
                  title: Text('info_site_web').tr(),
                  subtitle: Text('www.parcnationalhistorique.ht'),
                  onTap: (){
                    launch("http://www.parcnationalhistorique.ht");
                  },
                ),
              ),
            ]
          )
        ),
      ),
    );
  }
}