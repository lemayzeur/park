import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';
import 'package:park/colors.dart';
import 'package:park/screens/InfoScreen.dart';
import 'package:park/utils.dart';
import 'package:park/coords.dart';
import 'package:park/screens/SiteScreen.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> with WidgetsBindingObserver {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  static const LatLng _center = const LatLng(19.576763, -72.225357);
  LatLng _userLocation;
  int _screenNumber = 0;
  bool showAll = true;
  bool located = false;
  bool askingPermission = false;
  Set<Marker> _markers = {};
  bool isZoomed = false;
  Location location = new Location();
  Timer timer;

  MapType _currentMapType = MapType.satellite;
  final Set<Polyline> _polylines = {
    Polyline(
      polylineId: PolylineId('line1'),
      visible: true,
      points: coords,
      width: 2,
      color: Colors.yellow,
    )
  };

  final Set<Polyline> _large_polylines = {
    Polyline(
      polylineId: PolylineId('line2'),
      visible: true,
      points: large_coords,
      width: 3,
      color: Colors.yellow,
    )
  };

  Map<String, String> supportedLanguages = {
    'fr':'Français',
    'es':'Espagnol',
    'ht':'Créole',
    'en':'Anglais',
  };
  Map<String,BitmapDescriptor> icons;
  Map<String,BitmapDescriptor> iconsOnline;
  // void _onMarkerTapped(Marker marker) {
    
  // }

  Future getUserLocation() async {
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }
    final result = await location.getLocation();
    _userLocation = LatLng(result.latitude, result.longitude);
    return result;
  }
  
  getSetAddress(Coordinates coordinates) async {
    // final addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      // _resultAddress = addresses.first.addressLine;
    });
  }  
  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  // void _setmapStyle(GoogleMapController controller) async{
  //   String style = await DefaultAssetBundle.of(context).loadString('assets/map_style.json');
  //   controller.setMapStyle(style);
  // }

  void _setmapLabels(GoogleMapController thisController){
    int index = 0;
    timer = Timer.periodic(new Duration(seconds: 2), (timer) {
      try{
        thisController.showMarkerInfoWindow(MarkerId(index.toString()));
        index += 1;
        if(index > 3){
          index = 0;
        }
      }catch(e){
        print("Erreur: " + e.toString());
      }
    });
  }

  void _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
    _mapController = controller;
    _setLocalMapMarker().then((Map<String, BitmapDescriptor> output){
      setState(() {
        icons = output;
        _filterSite(null);
      });
    });

    _setOnlineMapMarker().then((Map<String, BitmapDescriptor> output){
      setState(() {
        iconsOnline = output;
        _filterSite(null);
      });
    });
    _setmapLabels(_mapController);
  }

  void _filterSite(String type) async{
    List<Map<String, dynamic>> filterSites = type != null ? sites.where((site) => site["type"] == type).toList() : sites;
    if(isZoomed){
      if(iconsOnline == null){
        _setOnlineMapMarker().then((Map<String, BitmapDescriptor> output){
          iconsOnline = output;
        });
      }
    }else{
      if(icons == null){
        _setLocalMapMarker().then((Map<String, BitmapDescriptor> output){
          icons = output;
        });
      }
    }
    
    Set<Marker> markers = filterSites.map((site) {
      LatLng position = new LatLng(site['lat'], site['lng']);
      print("ajout des markers");
      return Marker(
        markerId:MarkerId(site["id"].toString()),
        position: position,
        infoWindow: InfoWindow(
          title: tr(site['name']),
          onTap:()=> Navigator.push(context, MaterialPageRoute(
          builder: (context) => SiteScreen(site:site))),
        ),
        // icon: myIcon,
        // icon: BitmapDescriptor.defaultMarker,
        icon: isZoomed ? iconsOnline[site["id"].toString()] : icons[site['type']],
        onTap:()=> {}
      );
    }).toSet();
    // setState((){ _setmapLabels(_mapController);});
    setState((){ _markers = markers;});
    
  }

  Future<PermissionStatus> getLocationPermission() async {
    setState(() {
      askingPermission = true;
    });
    PermissionStatus result;
    final Location location = new Location();
    
    result = await location.hasPermission(); 
    if(result == PermissionStatus.denied){
      setState(() {
        askingPermission = false;
      });
    }
    return result;
  }

  Future<Uint8List> getBytesFromAsset(String path, int width, {bool isOnline: false}) async {
      // ByteData data = 
      ByteData data = isOnline ? await NetworkAssetBundle(Uri.parse(path)).load("") : await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
          targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui. ImageByteFormat.png)).buffer.asUint8List();
  }

  Future<Map<String,BitmapDescriptor>> _setLocalMapMarker({pin:true}) async {
    Map<String,BitmapDescriptor> output = {};
    final int size = 70;
    output["red"] = BitmapDescriptor.fromBytes(
      await getBytesFromAsset('assets/images/marker_red.png',size)
    );
    output["purple"] = BitmapDescriptor.fromBytes(
      await getBytesFromAsset('assets/images/marker_purple.png',size)
    );
    output["green"] = BitmapDescriptor.fromBytes(
      await getBytesFromAsset('assets/images/marker_green.png',size)
    );
    return output;
  }
  Future<Map<String,BitmapDescriptor>> _setOnlineMapMarker({pin:true}) async {
    Map<String,BitmapDescriptor> output = {};
    for(int i=1;i<=10;i++){
      output[i.toString()] = BitmapDescriptor.fromBytes(
        await getBytesFromAsset('https://c9hstatic.s3.amazonaws.com/unesco/${i.toString()}.png',
            82, isOnline: true)
      );
    }
    return output;
  }

  String _whichMarker(int index){
    switch(index){
      case 0:
        if(showAll)
          return null;
        return "red";
      case 1:
        return "green";
      case 2:
        return "purple";
      default:
        return  null;
    }
  }

  void _resetMap() async{
    GoogleMapController controller = await _controller.future;
      controller.setMapStyle("[]");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _resetMap();
    }
  }

  @override
  void initState() {
    getUserLocation();
    super.initState();
  }

  @override
  void dispose(){
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: secondaryColorDark,
          title: Text('app_name',
            style:TextStyle(fontSize:17.0, color:textColor)).tr(),
          automaticallyImplyLeading: true,
          leading: IconButton(icon: Icon(Icons.arrow_back, color:textColor),
            onPressed: (){Navigator.pop(context, false);}
          ),
          actions : <Widget>[
            IconButton(
              icon: Icon(
                Icons.info_outline,
                color:textColor,
              ),
              onPressed: (){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => InfoScreen()));
              }
            ),
            PopupMenuButton(
              color:bgColor,
              onSelected: (String choice){
                Locale newLocale = Locale(choice,choice == 'en' ? 'US':choice.toUpperCase());
                context.locale = newLocale;
              },
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context){
                return [
                  PopupMenuItem<String>(value: 'fr', child: Text('french').tr()),
                  PopupMenuItem<String>(value: 'ht', child: Text('creole').tr()),
                  PopupMenuItem<String>(value: 'es', child: Text('spanish').tr()),
                  PopupMenuItem<String>(value: 'en', child: Text('english').tr()),
                ];
              }
            )
          ]
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              polylines:  _polylines,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 12.67,
              ),
              zoomGesturesEnabled: true,
              onMapCreated: _onMapCreated,
              mapType: _currentMapType,
              onCameraMove: (CameraPosition cameraPosition){
                if(cameraPosition.zoom >= 12.9 && !isZoomed){
                  if(icons != null){
                    setState((){
                      isZoomed = true;
                    });
                    _filterSite(_whichMarker(_screenNumber));
                  }else{

                  }
                }else if (cameraPosition.zoom < 12.9 && isZoomed){
                  setState((){
                    isZoomed = false;
                  });
                  _filterSite(_whichMarker(_screenNumber));
                }
              },
              markers: _markers,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child:Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: <Widget>[
                    FloatingActionButton(
                      backgroundColor:_currentMapType == MapType.normal ? bgColor : secondaryColorDark,
                      foregroundColor:_currentMapType == MapType.normal ? secondaryColorDark : textColor,
                      heroTag:"btn1",
                      onPressed: _onMapTypeButtonPressed,
                      child: Icon(Icons.map, size: 36.0),
                    ),
                    SizedBox(height: 16.0),
                    FloatingActionButton(
                      onPressed: () async {
                        if (!located) {
                          if(_userLocation == null){
                            if(await getLocationPermission() == PermissionStatus.granted){
                              await getUserLocation();
                            }else{
                              AlertDialog(
                                title: Text("Localisation Désactivée"),
                                content: Text("Veuillez activer les services de localisation pour continuer."),
                                actions: [
                                FlatButton(
                                    child: Text("OK"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              );
                              return;
                            }
                          }
                          if(_userLocation != null){
                            setState(() {
                              _markers.add(
                                Marker(
                                  markerId: MarkerId(_userLocation.toString()),
                                  position: _userLocation,
                                  infoWindow: InfoWindow(
                                    title: tr('current_position')
                                  )
                                )
                              );
                              located = true;
                            });
                            
                            _mapController.animateCamera(CameraUpdate.newLatLng(_userLocation));
                          }   // _mapController.
                        }else if (located){
                          setState(() {
                            located = false;
                          });
                          _mapController.animateCamera(CameraUpdate.newLatLng(_center));
                        }
                      },
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor: bgColor,
                      heroTag:"btn2",
                      child: Icon(
                        located ? Icons.location_disabled : Icons.location_searching,
                        size: 36.0,
                        color: secondaryColorDark,
                      ),
                    ),
                  ],
                ), 
              )
            ),
            !showAll ? Positioned(
              left:16,
              bottom:16,
              child:FloatingActionButton(
                onPressed: () {
                  setState((){
                    _screenNumber = 0;
                    showAll = true;
                  });
                  _filterSite(null);
                },
                materialTapTargetSize: MaterialTapTargetSize.padded,
                backgroundColor: bgColor,
                child: const Icon(Icons.cancel, size: 30.0, color:secondaryColorDark),
              ),
            ) : Container()
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: bgColor,
          selectedItemColor: showAll ? const Color(0xFF777777) : primaryColorDark,
          unselectedItemColor: primaryColor,
          type: BottomNavigationBarType.fixed,
          items:[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('bottom_item_first').tr(),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.local_florist),
              title: Text('bottom_item_second').tr(),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.announcement),
              title: Text('bottom_item_third').tr(),
            )
          ],
          currentIndex: _screenNumber,
          onTap: (i){
            setState(() {
              _screenNumber = i;
              showAll = false;
            });
            _filterSite(_whichMarker(i));
          },
        )
      ),
    );
  }
}
