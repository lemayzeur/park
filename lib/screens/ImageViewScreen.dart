import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:park/colors.dart';

class ImageViewScreen extends StatelessWidget{
  final String imageUrl;
  final String info;
  bool isOnline = true;

  ImageViewScreen({@required this.imageUrl, this.info, this.isOnline});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: secondaryColorDark,
        title: Text(info),
      ),
      body: Container(
        color:Colors.black,
        child:Center(
          child: isOnline ? CachedNetworkImage(
            progressIndicatorBuilder: (context, url, progress) =>
                Center(
                  child: Container(
                    height:45.0, width:45.0,
                    child: CircularProgressIndicator(
                      value: progress.progress,
                    ),
                  ),
                ),
            imageUrl: imageUrl,
            errorWidget: (context, url, error) => const Icon(Icons.error),
            fadeOutDuration: const Duration(seconds: 1),
            fadeInDuration: const Duration(seconds: 2),
            fit: BoxFit.cover,
          ) : Image.asset(imageUrl)
        )
      ),
    );
  }
}