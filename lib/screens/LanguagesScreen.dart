import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:park/colors.dart';
import 'package:park/screens/HomeScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class LanguagesScreen extends StatelessWidget{
  Map<String, String> supportedLanguages = {
    'ht':'Kreyòl Ayisyen',
    'fr':'Français',
    'en':'English',
    'es':'Español',
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   elevation: 0.0,
      //   flexibleSpace: Padding(
      //     padding: const EdgeInsets.only(top: 25.0),
      //     child: Image(
      //       image: AssetImage("assets/images/top.png"),
      //       fit: BoxFit.fitWidth,
      //     ),
      //   ),
      //   backgroundColor: bgColor,
      // ),
      backgroundColor: bgColor,
      body: Padding(
        padding: const EdgeInsets.only(top: 25.0),
        child: Container(
          color: bgColor,
          child: new Column(
            children: [
              new Expanded(
                flex: 1,
                child: Image(
                  image: AssetImage("assets/images/top.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              new Expanded(
                flex: 8,
                child: Column( 
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: supportedLanguages.entries.map((entry){
                    return ButtonTheme(
                      minWidth: 75.0,
                      child: RaisedButton(
                        color: Colors.orangeAccent,
                        textColor: Colors.black,
                        // child: Text(entry.value + " (" + entry.key.toUpperCase() + ")",),
                        child: Container(
                          child: Image(
                            image: AssetImage("assets/images/flags/${entry.key}.jpg"),
                            height: 30.0,
                            width: 35.0,
                          ),
                        ),
                        onPressed: (){
                          print(entry.key);
                          Locale newLocale = Locale(entry.key,entry.key == 'en' ? 'US':entry.key.toUpperCase());
                          context.locale = newLocale;
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => HomeScreen()));
                        },
                      ),
                    );
                  }).toList(),
                ),
              ),
              new Expanded(
                flex: 1,
                child: Image(
                  image: AssetImage("assets/images/bottom.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ]
          ),
        ),
      )
    );
  }
}