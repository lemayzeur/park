import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:park/screens/HomeScreen.dart';
import 'package:park/screens/LanguagesScreen.dart';
import 'package:easy_localization/easy_localization.dart';

void main() {
  runApp(
    EasyLocalization(
      supportedLocales: [
        Locale('ht', 'HT'),
        Locale('fr', 'FR'),
        Locale('en', 'US'),
        Locale('es', 'ES'),
      ],
      path: 'lang',
      fallbackLocale: Locale('ht', 'HT'),
      useOnlyLangCode: true,
      child: ParkApp()
    )
  );
}

class ParkApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // color:const Color(0xFF4CAF50),
      title: 'Parc National Historique',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: LanguagesScreen(),
    );
  }
}

